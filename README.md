## Leaderboard-MK

**For official website visit: https://leaderboard.mapmykerala.in/**

**For Latest updates visit: https://mehmood15.gitlab.io/leaderboard-mk/**




  Mapathon keralam Leaderboard Application- An initiative of KSDI and ICFOSS. Displays live scores of mapathon campaign members which depends on individual OSM contributions.

    Copyright (C) 2007 Free Software Foundation   <http://fsf.org>

 This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

 This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.