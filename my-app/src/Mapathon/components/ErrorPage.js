import React, {useState} from 'react'
import Alert from "react-bootstrap/Alert"
import {Button} from "react-bootstrap"

function ErrorPage() {
    const [show, setShow] = useState(true)


    return (
        <div>
        <Alert show={show} variant = "success" onClose={() => setShow(false)} dismissible>
            <Alert.Heading>Sorry ! Please Try Again..</Alert.Heading>
            <hr />
            <p>
                <ul className="list-group-flush">
                    <li>
                        Check if url entered contains valid credentials
                    </li>
                    <li>
                        Check your Internet connection
                    </li>
                </ul>
            </p>
            
        </Alert>
        {!show && <Button onClick = {() =>  window.location.reload()}>Reload</Button>}

        </div>

    )
}

export default ErrorPage
