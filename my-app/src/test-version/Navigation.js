import React from 'react'
import {Navbar, Nav,Button} from "react-bootstrap"
import logo from "/home/ashiq/Documents/MAPATHON/frontendlearning_icfoss/my-app/src/assets/mapathon.png"

function Navigation(props) {
    return (
      <Navbar bg="light" expand="lg" variant="light">
         <Navbar.Brand href="#home">
            <img
            alt= ""
            src = {logo}
            width = "40"
            height = "40"
            className="d-inline-block align-top"
            />{' '}
            Mapathon Keralam
         </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
                <Nav.Link href="/" >Leaderboard</Nav.Link>
 
            </Nav>
                <Nav.Link href={props.route}>
                <Button variant="outline-success font-weight-bold">Log out</Button>
                </Nav.Link>
           
            </Navbar.Collapse>
      </Navbar>
    )
}

export default Navigation
