import React,{useState, useEffect} from "react"
import Navigation from "./Navigation"
import axios from "axios"
import List from "./ListGroups"
import UserPanel from "./UserPanel"
import LoadingComponent from "react-loading"
import jsonQ from "jsonq"
import CountUp from "react-countup"
import cup from "/home/ashiq/Documents/MAPATHON/frontendlearning_icfoss/my-app/src/assets/cup.jpg"
import {Container} from "react-bootstrap"
import {useSpring, animated} from 'react-spring'
import "./style.css"

function Dashboard() {
    //defining hooks
   
    const [user, setUser] = useState([])
    const [score, setScore] = useState([])
    const [load, setLoad] = useState(false)
    const [error, setError] = useState('')
    const [player, setPlayer] = useState('')
    const [campaign, setCampaign] = useState('')
    const [isHovered, setHovered] = useState(false)
   const [toggle , setToggle] = useState(false)
   
   //creating animations
    const props = useSpring({
        config: {duration: 400, mass:1, tension:50, friction:200},
        to: {transform: isHovered ? 'scale(1.03)' : 'scale(1)'    
         }
    })
    const user_prop= useSpring({
        config: {duration: 300, mass:1, tension:50, friction:50},
        to: {transform: isHovered ? 'scale(1.03)' : ' scale(1)'    
         },
         from : {transform: 'scale(1)'}
    })
    
   
    const winner = useSpring({
        to : {
        opacity: 1,
         transform :  load ?  'scale(1.05)' : ' scale(1)'
           
        } ,
        from : {opacity: 0, transform : 'scale(1)'}
    
    })
    const shadow_caster = useSpring({
        config: {duration: 400, mass:1, tension:50, friction:200},
        to: {transform: isHovered ? 'scaleX(0.80)' : 'scaleX(1)'},
        from : {transform : 'scaleX(1)'}
    })
    const fade_in = useSpring({
        config: {duration: 500, mass:1, tension:68, friction:1},
        to: {
             transform : load ? 'translate3d(0, 0, 0)' : 'translate3d(0, -40px, 0)' },
        from : {
                transform : 'translate3d(0,-40px,0)'}
    })

   
    
    
    /************************* */
    //conditional styles
    var normal_style={
        width:'50px'
    }
    var silver_style = { 
       fontSize: '1.3rem',
        color: '#2180bb',
        textShadow: '4px 2px 1px #C0C0C0'
  
    }
    var bronze_style = {
        fontSize: '1.3rem',
         color: '#ff6666',
         textShadow: '4px 2px 2px #ffcccc'
        
    }
    var ordinary = {
       fontSize: '1.2rem',
       color: '#505050'
    }
    var list_box = {
        border:'rgb(62, 204, 185)',
        borderRadius: '20px',
        padding:'4px',
        boxShadow: '10px 10px 20px 10px #C0C0C0'
    }
    var userHighlighter = { 
        border:'1px solid rgb(62, 204, 185)',
        borderRadius: '20px',
        padding:'4px',
        boxShadow: '10px 10px 20px 10px #092b24',
        
    }
    var url = window.location.pathname
   

    
    useEffect(() => {
        axios.all([
            axios.get('https://my-json-server.typicode.com/AshiqMehmood/demo/profile'),
            //axios.get('http://192.168.48.45:5000/api/user_details/test-campaign/Arun%20Madhavan'),
            //axios.get('http://192.168.48.45:5000/api/campaign_details/test-campaign'),
            //axios.get(`http://192.168.48.45:5000/api/campaign_details/test-campaign`),
            axios.get('https://my-json-server.typicode.com/AshiqMehmood/demo/posts')
        ])
        //url based importing data {{ to import local add ('./scoreData.json') }}}
        .then(axios.spread(function(userDetails, score) {
                setScore(score.data)
                setUser(userDetails.data)
                
                let x=[]
                x= userDetails.data
                for(var i=0; i<=x.length;i++){
                    var obj = x[i]
                    //console.log(obj.name)
                    setPlayer(obj.display_name)
                    setCampaign(obj.campname)
                    
                }
                
              
              //console.log(userDetails.changeset_details)
             // console.log(score.data)
              setLoad(true)
              
              //console.log(score.data)
            })) 
                 //pass the fetched json data response to a
                                    //array of state in order to acces it in render() method
                //console.log(this.state.scoreboard)  
              
                //var obj = Object.values(response.data)
               // var ans = obj[Math.floor(Math.random() * obj.length)]
               // console.log(ans)
           
            .catch(err => {
                setError(err.message);
                setLoad(true)
            })

        }, [])
        
        //console.log(user.changeset_id)
       const sorted_data = score.sort(GetSortOrder("score")) //sort list grop in decreasing order
      
      /* const mainData = sorted_data.map((item) => {
           if(item.display_name === player){
            return <List
            key={item.osm_id}
            player={item.display_name}
            score={<CountUp
                 start={0}
                 end={item.score}
                 duration={item.score >=1000 ? 4 : 8}
                 />}
            osmID={item.osm} 
            style={userHighlighter} 
            rankStyle={ordinary}
            rank={<PositionList pos={item.score} array={sorted_data}/>}
         />

           }
            else {
            return <List
            key={item.id}
            player={item.display_name}
            score={<CountUp
                 start={0}
                 end={item.score}
                 duration={item.score >=1000 ? 5 : 9}
                 />}
            osmID={item.osm}                 
            rankStyle={rank_validator(item.score)}
            style={list_box}
            rank={<PositionList pos={item.score} array={sorted_data}/>}
         />

           }
            
        })*/
      

        const userData = user.map((item) => {
              return <UserPanel 
                key={item.id}
                name={item.display_name}
                score={<CountUp
                 start={0}
                 end={item.score}
                 duration={item.score >=1000 ? 4 : 8}
                 />}
                osmID={item.osm}
                campID={item.camp}
                campName={item.campname}
                rank={<PositionList pos={item.score}/>}
            />            
        })

        function rank_validator(point){
            let data = jsonQ(sorted_data)
             let filtered_data = data.find('score')
            let arr = filtered_data.value()
            let index_arr = arr.indexOf(point) + 1
        
            if(index_arr === 1){
                return true
            }
            else if(index_arr === 2){
                return silver_style
            }
            else if(index_arr === 3){
                return bronze_style
            }
            else{
                return ordinary
            }
                

        }
        function validator(point){
            let data = jsonQ(sorted_data)
             let filtered_data = data.find('score')
            let arr = filtered_data.value()
            let index_arr = arr.indexOf(point) + 1
            return index_arr

        }
       
        

        function GetSortOrder(prop){ //sort json data in Descending order
            return function(a,b){
                if(a[prop] > b[prop]){
                    return -1
                }
                else if(a[prop] < b[prop]){
                    return 1
                }
                return 0
            }
        }  
        function PositionList({pos}) {
            let data = jsonQ(sorted_data)
                    let filtered_data = data.find('score')
                    let arr = filtered_data.value() //return array of scores from total json data [16222, 7000, 2001,99, 22, 2...]
                    //console.log(arr)  
                    var new_arr = arr.filter((value) => {
                        return value !== pos
                    })
                     // console.log(new_arr)
                    let index_arr = arr.indexOf(pos) //returns the absolute position of a particular score in the array
                    let position = index_arr + 1    //add 1 to make it a score figure
                    //let position_last = index_arr
                    //console.log(position)
        
                    return <span>{position}</span> /*here indexOf returns the position of score in the array 
                                                      even if the scores are same, we can return same value */
        }
        

if(load){
    return (
        <div >
       
            <Navigation  route="/"/>
           
            <Container>
            <div className="d-block text-center"><h3 className="header font-weight-bold m-2 p-2">Campaign </h3></div>
               <div className="row justify-content-center">
                    
                    <div className="scrollbar scrollbar-primary col-lg-10 col-md-9 col-sm-12 col-xl-7" >
                     
                      {
                        sorted_data.map((item) => {
                            if(item.display_name === player){
                            return <animated.div style={user_prop}><List
                            key={item.id}
                            player={item.display_name}
                            score={<CountUp
                                start={0}
                                end={item.score}
                                duration={item.score >=1000 ? 4 : 8}
                                />}
                            osmID={item.osm} 
                            style={userHighlighter} 
                            rankStyle={ordinary}
                          
                            rank={<PositionList pos={item.score} array={sorted_data}/>}
                        /></animated.div>

                        }
                        else if(validator(item.score) === 1){
                            return <animated.div style={winner}> <List
                            key={item.id}
                            player={item.display_name}
                            score={<CountUp
                                start={0}
                                end={item.score}
                                duration={item.score >=1000 ? 5 : 9}
                                />}
                            osmID={item.osm}                 
                            rankStyle={rank_validator(item.score)}
                            style={list_box}
                            rank={<PositionList pos={item.score} array={sorted_data}/>}
                        /></animated.div>
                        }
                            else {
                            return <animated.div style={fade_in}> <List
                            key={item.id}
                            player={item.display_name}
                            score={<CountUp
                                start={0}
                                end={item.score}
                                duration={item.score >=1000 ? 5 : 9}
                                />}
                            osmID={item.osm}                 
                            rankStyle={rank_validator(item.score)}
                           
                            style={list_box}
                            rank={<PositionList pos={item.score} array={sorted_data}/>}
                        /></animated.div>

                        }
                            
                        })
                      }
                        
                    </div>
                
               <div className="col-lg-10 col-sm-12 col-md-10 col-xl-5 order-first order-md-0" 
                     onMouseEnter={() => setHovered(true)}
                     onMouseLeave={() => setHovered(false)}
                    > 
                    <div className="row ">
                     <div className="col-lg-12">
                         <animated.div style={props}> {userData}</animated.div> 
                     </div> 
                     <div className="col-lg-12 d-flex justify-content-center">
                        <animated.div style={shadow_caster} className="card-shadow"></animated.div>
                     </div>
                    </div> 
                    
                   
               </div>
                    

                    
            </div>       
                
            

            </Container>
      
        </div>
        
    )
}
else{
    return <div><LoadingComponent type="bars" color="black" className="w3-display-middle"/></div>
 }
}

export default Dashboard
