import React from 'react'

import {BrowserRouter, Route, Switch} from "react-router-dom"
import dashboard from "./Dashboard"

function App() {
    return (
        <div>
           
            <BrowserRouter>
                <Switch>
            
                    <Route path="/test-campaign" component={dashboard} />
                </Switch>

            </BrowserRouter>
            
        </div>
    )
}

export default App
