import React,{useState, useEffect} from "react"
import {useMediaQuery} from "react-responsive"
import jsonQ from "jsonq"
import CountUp from "react-countup"
import {useSpring, animated} from "react-spring"
import {useLocation} from "react-router-dom"
import List from "../components/ListGroups"
import {Container} from "react-bootstrap"
import axios from "axios"
import Header from "../components/Header"
import WinnerPanel from "./WinnersPanel"
import MobWinnerPannel from "./MobwinnerPanel"
import LoaderPage from "../loaderPage/mapLogoAnime"
import ErrorPage from "../components/ErrorPage"
import Gold from "assets/cup.svg"
import Silver from "assets/silver.svg"
import Bronze from "assets/bronze.svg"
import Bg from "../components/particleBg"
import Campaign from "../components/CampName"
import "./leaderboard.css"

function Leaderboard(){

  const [listdata, setListdata] = useState([])
  //const [campaign , setCampaign] = useState("Campaign Name")
  const [load, setLoad] = useState(false)
  const [error, setError] = useState(false)
  const isMobile = useMediaQuery({//bug #101: Do not work with half browser window
    query: '(max-width: 760px) '  //bug fixed  [media-device-width work only for resizing acc. to each device resolution]
  })
  const isLargeScreen = useMediaQuery({
    query: '(min-device-width: 1900px)'
  })





var silver_style = { 
  fontSize: '1.4rem',
   color: '#2180bb'
  

}
var bronze_style = {
   fontSize: '1.4rem',
    color: '#ff6666'
   
   
}
var ordinary = {
  fontSize: '1.3rem',
  color: '#505050'
}
var list_box = {
   borderRadius: '20px',
   padding:'4px',
   boxShadow: '5px 4px 3px 3px #C0C0C0'

}
//animations
const winner = useSpring({
    config: {duration: 2300, mass:10, tension:50, friction:10},
    to : {
     opacity: 1,
     transform :  load ?  'scale(1.06)' : ' scale(1)'      
    } ,
    from : {opacity: 0, transform : 'scale(1)'}
})
const start_ease= useSpring({
    config: {duration: 1400, mass: 250, tension: 30, friction: 10},
    to: { opacity: load ?  1 : 0},
    from :{ opacity: 0}
})
const {pathname} = useLocation()
var get_url =  pathname     //to obtain path from browser window
var data_slicer = get_url.slice(11,)    //remove the base url i.e landing page (/dashboard)
var data_campaign = data_slicer.split("/")  //convert string to array for selecting individual parameters

//https://my-json-server.typicode.com/AshiqMehmood/demo/
//https://mapathon.icfoss.org/LeaderBoard/api/campaign_details/

 useEffect(() => {
  axios({
    method: 'get',
    baseURL : 'https://my-json-server.typicode.com/AshiqMehmood/demo/' + data_campaign[0],
    timeout: '10000'
    
  })
  .then(response => {
    //console.log(response.data)
    //console.log(Object.keys(response.data)[0])
    setListdata(response.data)
    //setCampaign(Object.keys(response.data)[0])
    setLoad(true)
    

  })
  .catch(err => {
   console.log(err)
  
    setError(true)
  })
 }, [])


//var results = score
const sorted_data = listdata.sort(GetSortOrder("score")) //sorted  list of whole team data

const winners_list = sorted_data.map((arr) => { //reduce sorted whole list to winners only list
  if(validator(arr.score) <= 3){
    return arr
  }
})


function rank_validator(point){ //function to provide dynamic Rank Style based on ranks
  let data = jsonQ(sorted_data)       
   let filtered_data = data.find('score')
  let arr = filtered_data.value()
  let index_arr = arr.indexOf(point) + 1

  if(index_arr === 1){
      return true //this condition is checked in component 'ListGroups' to render crown
  }
  else if(index_arr === 2){
      return silver_style
  }
  else if(index_arr === 3){
      return bronze_style
  }
  else{
      return ordinary
  }
      
}
function validator(point){ //function returns the position of incoming scores & allows for future conditions
  let data = jsonQ(sorted_data)
   let filtered_data = data.find('score')
  let arr = filtered_data.value()
  let index_arr = arr.indexOf(point) + 1
  return index_arr

}

function GetSortOrder(prop){ //sort json data in Descending order
  return function(a,b){
      if(a[prop] > b[prop]){
          return -1
      }
      else if(a[prop] < b[prop]){
          return 1
      }
      return 0
  }
}  

function PositionList({pos, array}) { //used for correct Rank Computation
  let data = jsonQ(array)
          let filtered_data = data.find('score')
          let arr = filtered_data.value() //return array of scores from total json data [16222, 7000, 2001,99, 22, 2...]
          var new_arr = arr.filter((value) => {
              return value !== pos
          })
          let index_arr = arr.indexOf(pos) //returns the absolute position of a particular score in the array
          let position = index_arr + 1    //add 1 to make it a score figure
          //let position_last = index_arr 
          return <span>{position}</span> /*here indexOf returns the position of score in the array  even if the scores are same, we can return same value */
}

    if(load){
    return(
        
      <animated.div style={start_ease}>

        <Header />
        
       <Container className="backg">
        
         <Campaign name={data_campaign[0]}/>

        <div className="row justify-content-center">
          <div className="col-lg-6 col-sm-12 col-md-12 col-xl-6">
            <div className="winners-box " style={isLargeScreen ? { height: '760px'} : { height: '520px'}}>
              <div className="text-center m-2 p-2" 
                style={isLargeScreen ? {fontSize: '2rem', color:'#ff80df',fontFamily: '"Courier New", Courier, monospace'} : {fontSize: '1.5rem', color:'#ff80df',fontFamily: '"Courier New", Courier, monospace'}}>
                TOP ACHIEVEMENTS</div>
                {
                  winners_list.map((item, index) => {
                    if(item !== undefined){
                     // console.log(item)   
                      return (
                        <div>
                        {isMobile
                        ?   <MobWinnerPannel
                             key={index}
                             name={item.display_name}
                             score={item.score} 
                             cup={validator(item.score) === 1 ? Gold : validator(item.score) === 2 ? Silver : Bronze}
                             rank={<PositionList pos={item.score} array={winners_list}/>}
                             super={validator(item.score) === 1 ? 'st' : validator(item.score) === 2 ? 'nd' : 'rd'}
                            /> 

                        :  <WinnerPanel
                             name={item.display_name}
                             score={item.score} 
                             cup={validator(item.score) === 1 ? Gold :  validator(item.score) === 2 ? Silver : Bronze}
                             rank={<PositionList pos={item.score} array={winners_list}/>}
                             super={validator(item.score) === 1 ? 'st' : validator(item.score) === 2 ? 'nd' : 'rd'}
                        />}
                       
                        </div>   
                      )
                     } 
                  })
                  
                }
             </div>
            </div>
          
     
          <div className="scrollbox scrollbox-primary col-lg-6 col-sm-12 col-md-12 col-xl-6"  style={isLargeScreen ? { height: '760px'} : { height: '520px'}}>
              
              <div >
              
              {sorted_data.map((item,index) => {  
                  //console.log(index)             
                         if(validator(item.score) === 1){
                            return <animated.div style={winner}> 
                            <List
                            key={item.osm_id}
                            player={item.display_name}
                            score={<CountUp
                                start={0}
                                end={item.score}                   
                                duration={item.score >=1000 ? 1 : 2}
                                />}
                            osmID={item.osm_id}                 
                            rankStyle={rank_validator(item.score)}
                            style={list_box}
                            rank={<PositionList pos={item.score} array={sorted_data}/>}
                            
                        /></animated.div>
                        }
                            else {
                              
                            return <div > <List
                            key={item.osm_id}
                            player={item.display_name}
                            score={<CountUp
                                start={0}
                                end={item.score}
                                duration={item.score >=1000 ? 1 : 2}
                                />}
                            osmID={item.osm_id}                 
                            rankStyle={rank_validator(item.score)}
                           
                            style={list_box}
                            rank={<PositionList pos={item.score} array={sorted_data}/>}
                           
                        /></div>
                  }
              })
              }
                
                </div>
            
          </div>
            
          </div>
        
        

        
     
       </Container> 
       <Bg />
       </animated.div>       
    )}
    else if(error){
      return <ErrorPage />
    }

    else 
    return <LoaderPage />
}

export default Leaderboard
