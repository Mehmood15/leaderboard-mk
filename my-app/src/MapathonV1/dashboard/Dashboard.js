import React,{useState, useEffect} from "react"
import {useSpring, animated} from 'react-spring'
import {useLocation} from "react-router-dom"
import {useMediaQuery} from "react-responsive"
import axios from "axios"
import jsonQ from "jsonq"
import CountUp from "react-countup"
import {Container} from "react-bootstrap"
import LoaderPage from "../loaderPage/mapLogoAnime"
import Header from "../components/Header"
import List from "../components/ListGroups"
import UserPanel from "./UserPanel"
import ErrorPage from "../components/ErrorPage"
import Campaign from "../components/CampName"
import Bg from "../components/particleBg"

function Dashboard() {
    //defining hooks
    const isLargeScreen = useMediaQuery({
        query: '(min-device-width: 1900px)'
      })    
    const isDesktop = useMediaQuery({
        query: '(min-device-width: 1600px)'
    })      
      

    const [user, setUser] = useState([])
    const [score, setScore] = useState([])
    //const [dashboard, setDashboard] = useState(false)
    const [load, setLoad] = useState(false)
    const [error, setError] = useState(false)
    const [player, setPlayer] = useState(" ")
    const [campaign, setCampaign] = useState(" ")
    const [isHovered, setHovered] = useState(false)
   
   
   //creating animations
   const start_ease= useSpring({
       config: {duration: 1000, mass: 50, tension: 30, friction: 10},
       to: { opacity: load ?  1 : 0},
       from :{ opacity: 0}
   })
    const card_scale = useSpring({
        config: {duration: 400, mass:10, tension:250, friction:100},
        to: {transform: isHovered ? 'scale(1.05)' : 'scale(1)'    
         }
    })
    const user_prop= useSpring({
        config: {duration: 400, mass:10, tension:150, friction:1},
        to: {transform: isHovered ? 'scale(1.05)' : ' scale(1)'    
         },
         from : {transform: 'scale(1)'}
    })
   
    const winner = useSpring({
        config: {duration: 2300, mass:10, tension:50, friction:10},
        to : {
        opacity: 1,
         transform :  load ?  'scale(1.05)' : ' scale(1)'
           
        } ,
        from : {opacity: 0, transform : 'scale(1)'},
    
    })

    const shadow_caster = useSpring({
        config: {duration: 300, mass:1, tension:100, friction:50},
        to: {transform: isHovered ? 'scaleX(0.80)' : 'scaleX(1)'},
        from : {transform : 'scaleX(1)'}
    })

   
    
    
    /************************* */
    //conditional styles
    let head_style = {
        normal : {color:'#008040', fontSize: '2rem', fontWeight: 'bold' , fontFamily: '"Courier New", Courier, monospace'},
        large : {color:'#008040', fontSize: '2.8rem',  fontWeight: 'bold',  fontFamily: '"Courier New", Courier, monospace'}
      }

    var normal_style={
        width:'50px'
    }
    var silver_style = { 
       fontSize: '1.3rem',
        color: '#2180bb',
  
    }
    var bronze_style = {
        fontSize: '1.3rem',
         color: '#ff6666',
        
    }
    var ordinary = {
       fontSize: '1.2rem',
       color: '#505050'
    }
    var list_box = {
        borderRadius: '20px',
        padding:'4px',
        boxShadow: '5px 4px 3px 3px #C0C0C0'
    }
    var userHighlighter = { 
        border:'1px solid #6ce0c9',
        borderRadius: '20px',
        padding:'4px',
        boxShadow: '2px 5px 3px 1px #092b24',
        
    }
   const { pathname } = useLocation()
    var get_url = pathname //to obtain path from browser window
    var data_slicer = get_url.slice(11,)    //remove the base url i.e landing page (/dashboard)
    var data_campaign = data_slicer.split("/")  //convert string to array for selecting individual parameters
    
    
    useEffect(() => {
      // console.log(data_campaign)
      //'https://mapathon.icfoss.org/LeaderBoard/api/user_details/' + data_campaign[0] + '/' + data_campaign[1]
      //'https://mapathon.icfoss.org/LeaderBoard/api/campaign_details/' + data_campaign[0]
      //https://my-json-server.typicode.com/AshiqMehmood/demo/posts
      
           axios.all([ //when multiple axios call
            
            axios({
                method: 'get', //for user details
                baseURL:  'https://mapathon.icfoss.org/LeaderBoard/api/user_details/' + data_campaign[0] + '/' + data_campaign[1],
                timeout : '15000'   
 
            }),
            axios({
                method: 'get', //for list campaign details
                baseURL: 'https://mapathon.icfoss.org/LeaderBoard/api/campaign_details/' + data_campaign[0],
                timeout : '15000'  
            })
           ])
            .then(axios.spread(function(userDetails, score){ //parameters are responses from corresponding axios calls
                setLoad(true)
                setUser(userDetails.data)
                setScore(score.data)
                //setCampaign(Object.keys(response.data)[0])  //if parent name of api contains 'Campaign Name'
                 console.log(userDetails.data)
                 var x=[]
                 var obj = ""
                 if (obj !== undefined) {
                    // Now we know that foo is defined, we are good to go.
                    x= userDetails.data
                    for(var i=0; i<=x.length;i++){
                         obj = x[i] 
                        setPlayer(obj.name)               
                    }
                }
               
            }))
            .catch(error => {
                console.log(error)
                setError(true)
            })
            
       }, [])
     
       const sorted_data = score.sort(GetSortOrder("score")) //sort list grop in decreasing order

        const userData = user.map((item) => { //mapping User data for siplay
              return <UserPanel 
                key={item.osm_id}
                name={item.name}
                score={<CountUp
                 start={0}
                 end={item.score}
                 duration={item.score >=1000 ? 1 : 2}
                 />}
                osmID={item.osm_id}  
                campName={item.campaign_name}
                rank={<PositionList pos={item.score}/>}
            />            
        })

        function rank_validator(point){ //function to provide dynamic Rank Style based on ranks
            let data = jsonQ(sorted_data)       
             let filtered_data = data.find('score')
            let arr = filtered_data.value()
            let index_arr = arr.indexOf(point) + 1
        
            if(index_arr === 1){
                return true //this condition is checked in component 'ListGroups' to render crown
            }
            else if(index_arr === 2){
                return silver_style
            }
            else if(index_arr === 3){
                return bronze_style
            }
            else{
                return ordinary
            }
                

        }
        function validator(point){ //function returns the position of incoming scores & allows for future conditions
            let data = jsonQ(sorted_data)
             let filtered_data = data.find('score')
            let arr = filtered_data.value()
            let index_arr = arr.indexOf(point) + 1
            return index_arr

        }
       
        

        function GetSortOrder(prop){ //sort json data in Descending order
            return function(a,b){
                if(a[prop] > b[prop]){
                    return -1
                }
                else if(a[prop] < b[prop]){
                    return 1
                }
                return 0
            }
        }  
        function PositionList({pos}) { //used for correct Rank Computation
            let data = jsonQ(sorted_data)
                    let filtered_data = data.find('score')
                    let arr = filtered_data.value() //return array of scores from total json data [16222, 7000, 2001,99, 22, 2...]
                    var new_arr = arr.filter((value) => {
                        return value !== pos
                    })
                    let index_arr = arr.indexOf(pos) //returns the absolute position of a particular score in the array
                    let position = index_arr + 1    //add 1 to make it a score figure
                    //let position_last = index_arr 
                    return <span>{position}</span> /*here indexOf returns the position of score in the array  even if the scores are same, we can return same value */
        }
        

if(load ){ //checks if all axios calls are complete
    return (
        <animated.div style={start_ease} >
           <Header />
            <Container className="backg">
                <Campaign name ={data_campaign[0]} />

               <div className="row justify-content-center">
               <div className=" col-lg-10 col-sm-12 col-md-10 col-xl-5 order-first order-md-0" 
                     onMouseEnter={() => setHovered(true)}
                     onMouseLeave={() => setHovered(false)}
                     
                    > 
                    <div className="row ">
                     <div className="col-lg-12 pb-3">
                         <animated.div style={card_scale}> {userData}</animated.div> 
                     </div> 
                     
                     <div className="col-lg-12 d-flex justify-content-center">
                        <animated.div style={shadow_caster} className="card-shadow"></animated.div>
                     </div> 
                     
                    
                    </div> 
                    
                   
               </div>
                    
                 <div className="scrollbar scrollbar-primary col-lg-10 col-md-9 col-sm-12 col-xl-7"  style={isLargeScreen ? { height: '760px'} : {height: '510px'}}>
                     
                      {
                        sorted_data.map((item) => {
                            if(validator(item.score) === 1 && item.display_name === player){
                            return <animated.div style={winner}><List
                            key={item.osm_id}
                            player={item.display_name}
                            score={<CountUp
                                start={0}
                                end={item.score}
                                duration={item.score >=1000 ? 1 : 1.5}
                                />}
                            osmID={item.osm_id} 
                            style={userHighlighter} 
                            rankStyle={rank_validator(item.score)}
                          
                            rank={<PositionList pos={item.score} array={sorted_data}/>}
                        /></animated.div>
  
                        } 
                           else if(item.display_name === player){
                            return <animated.div style={user_prop}><List
                            key={item.osm_id}
                            player={item.display_name}
                            score={<CountUp
                                start={0}
                                end={item.score}
                                duration={item.score >=1000 ? 1 : 1.5}
                                />}
                            osmID={item.osm_id} 
                            style={userHighlighter} 
                            rankStyle={rank_validator(item.score)}
                          
                            rank={<PositionList pos={item.score} array={sorted_data}/>}
                        /></animated.div>

                        }
                      
                        else if(validator(item.score) === 1){
                            return <animated.div style={winner}>
                             <List
                            key={item.osm_id}
                            player={item.display_name}
                            score={<CountUp
                                start={0}
                                end={item.score}                   
                                duration={item.score >=1000 ? 1 : 2}
                                />}
                            osmID={item.osm_id}                 
                            rankStyle={rank_validator(item.score)}
                            style={list_box}
                            rank={<PositionList pos={item.score} array={sorted_data}/>}
                        /></animated.div>
                        }
                            else {
                            return <div> <List
                            key={item.osm_id}
                            player={item.display_name}
                            score={<CountUp
                                start={0}
                                end={item.score}
                                duration={item.score >=1000 ? 1 : 2}
                                />}
                            osmID={item.osm_id}                 
                            rankStyle={rank_validator(item.score)}
                           
                            style={list_box}
                            rank={<PositionList pos={item.score} array={sorted_data}/>}
                        /></div>

                        }
                            
                        })
                      }
                        
                    </div>
                
               
                    

                    
            </div>       
                
            </Container>
                      
           <Bg />           
        </animated.div>
        
    )
}
else if(error){
    return <ErrorPage />
}

else{
    return <div><LoaderPage /></div>
 }
}

export default Dashboard
